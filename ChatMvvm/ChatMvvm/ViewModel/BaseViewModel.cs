﻿using ChatMvvm.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMvvm.ViewModel
{
    //The ViewModelBase abstract class is designed to serve as a base class for all ViewModel classes. 
    //It provides support for property change notifications. 
    //Instead of implementing fx. the INotifyPropertyChanged interface in each individual ViewModel of your application, 
    //you can directly inherit the ViewModelBase class
    public class BaseViewModel
    {
        protected readonly IWebService service = ServiceContainer.Resolve<IWebService>();
        protected readonly ISettings settings = ServiceContainer.Resolve<ISettings>();

        public event EventHandler IsBusyChanged = delegate { };

        private bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                IsBusyChanged(this, EventArgs.Empty);
            }
        }
    }
}
