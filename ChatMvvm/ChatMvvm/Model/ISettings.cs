﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMvvm.Model
{
    //A simple interface for persisting application settings
    public interface ISettings
    {
        User User { get; set; }

        void Save();
    }
}
