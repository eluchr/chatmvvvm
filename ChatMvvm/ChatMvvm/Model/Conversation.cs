﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMvvm.Model
{
    public class Conversation
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string LastMessage { get; set; }
    }
}
