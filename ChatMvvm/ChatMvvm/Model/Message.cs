﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMvvm.Model
{
    public class Message
    {
        public int Id { get; set; }
        public int ConversationId { get; set; }
        public int UserId { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public bool Incoming { get; set; }
    }
}
