﻿using ChatMvvm.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMvvm.FakeModel
{
    public class FakeSettings : ISettings
    {
        public User User { get; set; }
        public void Save() { }
    }
}
