﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ChatMvvm.ViewModel;

namespace ChatMvvm.Droid.Activities
{
    [Activity(Label = "BaseActivity")]
    public class BaseActivity<TViewModel> : Activity where TViewModel : BaseViewModel
    {
        protected readonly TViewModel viewModel;
        protected ProgressBar progressBar;

        public BaseActivity()
        {
            viewModel = ServiceContainer.Resolve(typeof(TViewModel)) as TViewModel;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            //progressBar = new ProgressBar(this);
            //progress.SetCancelable(false); Sets whether this dialog is cancelable with the BACK key.
            //progressBar.SetTitle(Resource.String.Loading);
        }
        protected override void OnResume()
        {
            base.OnResume();
            viewModel.IsBusyChanged += OnIsBusyChanged;
        }

        protected override void OnPause()
        {
            base.OnPause();
            viewModel.IsBusyChanged -= OnIsBusyChanged;
        }

        void OnIsBusyChanged(object sender, EventArgs e)
        {
            //if (viewModel.IsBusy)
            //    progressBar.setVisibility(View.VISIBLE);  //To show ProgressBar
            //else
            //    progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        }

        protected void DisplayError(Exception exc)
        {
            string error = exc.Message;
            //new AlertDialog.Builder(this)
            //    .SetTitle(Resource.String.ErrorTitle)
            //    .SetMessage(error)
            //    .SetPositiveButton(Android.Resource.String.Ok,
            //        (IDialogInterfaceOnClickListener)null)
            //    .Show();
        }
    }
}